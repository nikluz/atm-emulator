$(document).ready(function() {
    var OnDigitalKeyboardClick = function(event) {
        event.preventDefault();

        var field = $(this).closest('.field_with_digital_kb');
        var input_field = field.find('input');
        var output_field = field.find('.field_output');

        if(input_field.attr('name') == 'card_number') {
            if(input_field.val().length >= 16) return;
        } else if(input_field.attr('name') == 'pin_code') {
            if(input_field.val().length >= 4) return;
        }


        var new_val = input_field.val() + $(this).data('key');

        input_field.val(new_val);
        formatDigitalKeyboardOutputField(field);
    }

    $(document).on('click', '.digital_kb button', OnDigitalKeyboardClick);

    var OnFormResetClick = function() {
        $(this).find('.field_with_digital_kb').each(function() {
            var input_field = $(this).find('input');
            var output_field = $(this).find('.field_output');
            input_field.val('');
            output_field.text('');
        });
    }

    $(document).on('reset', 'form', OnFormResetClick);


    if($('.field_with_digital_kb').length > 0) {
        $('.field_with_digital_kb').each(function() {
            formatDigitalKeyboardOutputField($(this));
        });
    }
});

var formatDigitalKeyboardOutputField = function(field) {
    var input_field = field.find('input');
    var output_field = field.find('.field_output');
    var new_val = input_field.val();

    if(input_field.attr('name') == 'card_number') {
        new_val = formatCreditCard(new_val);
    } else if(input_field.attr('name') == 'pin_code') {
        new_val = new Array( new_val.length + 1 ).join('*');
    }
    output_field.text(new_val);
}

var formatCreditCard = function(card_number) {
    card_number = card_number.replace(/[^0-9]/gi, '');
    parts = card_number.match(/\d{1,4}/g) || [];
    if (parts.length) {
        return parts.join('-')
    } else {
        return card_number
    }
}