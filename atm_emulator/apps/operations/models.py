# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models.signals import post_save


class AbstractOperation(models.Model):
    """
    Абастрактная модель операций

    Поля:
        card - банковской карта
        date_time - дата/время проведения операции
        operation_code - код операции, который генерируется из id
    """

    card = models.ForeignKey('main.Card', verbose_name=u"Банковская карта")
    date_time = models.DateTimeField(auto_now=True, verbose_name=u"Дата")
    operation_code = models.CharField(verbose_name=u'Код операции',
                                      max_length=64,
                                      null=True, unique=True)

    @staticmethod
    def post_save(sender, instance, **kwargs):
        if instance.operation_code is None:
            instance.operation_code = str(instance.pk).zfill(8)
            instance.save()

        return instance

    def __unicode__(self):
        return "%s (%s)" % (
            self.card.card_number_formatted,
            self.date_time,
        )

    class Meta:
        abstract = True


class ViewBalanceOperation(AbstractOperation):
    """
    Операции просмотра баланса

    Поля:
        Все поля из Абстрактной модели операций
    """

    class Meta:
        verbose_name = u"операция просмотра баланса"
        verbose_name_plural = u"операции просмотра баланса"


post_save.connect(ViewBalanceOperation.post_save, ViewBalanceOperation)


class GetMoneyOperation(AbstractOperation):
    """
    Операции просмотра баланса

    Поля:
        Все поля из Абстрактной модели операций
        +
        amount - кол-во снятых средств
    """

    amount = models.PositiveIntegerField(verbose_name=u'Баланс', default=0)

    def __unicode__(self):
        return "%s На сумму: %s" % (
            super(GetMoneyOperation, self).__unicode__(),
            self.amount,
        )

    class Meta:
        verbose_name = u"операция снятия средств"
        verbose_name_plural = u"операции снятия средств"


post_save.connect(GetMoneyOperation.post_save, GetMoneyOperation)
