# -*- coding: utf-8 -*-
from django.views.generic import TemplateView, View
from django.views.generic.edit import FormView
from django.views.generic.detail import SingleObjectMixin
from main.models import Card
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
from django.db.transaction import atomic

from operations.models import ViewBalanceOperation, GetMoneyOperation
from operations.forms import GetMoneyForm
from common.utils import redirect_to_messages_url


class CardMixin(object):
    """ Проверка card_pk из сессии """

    def dispatch(self, request, *args, **kwargs):
        card_pk = self.request.session.get('card_pk', None)
        self.card = None

        if card_pk is not None:
            self.card = Card.objects.filter(id=card_pk).first()

        if self.card is None:
            messages.error(self.request,
                           u'Карта не авторизирована')
            return redirect_to_messages_url(reverse('homepage'))
        return super(CardMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CardMixin, self).get_context_data(**kwargs)
        context['card'] = self.card
        return context


class OperationsLogout(View):
    """ Выход из операций """

    def get(self, request, *args, **kwargs):
        if 'card_pk' in request.session:
            del self.request.session['card_pk']
        messages.success(self.request,
                         u'Вы вышли из операций')
        return redirect('homepage')


class OperationsListView(CardMixin, TemplateView):
    """ Страница со списком операций """

    template_name = "operations/operations_list.html"


class BalanceView(CardMixin, TemplateView):
    """ Страница просмотра баланса """

    template_name = "operations/balance.html"

    def get(self, request, *args, **kwargs):
        ViewBalanceOperation.objects.create(card=self.card)
        return super(BalanceView, self).get(request, *args, **kwargs)


class GetMoneyView(CardMixin, FormView):
    """ Страница с формой для снятия средств """

    template_name = 'operations/get_money_form.html'
    form_class = GetMoneyForm
    success_url = 'operations:get_money_success'
    fail_url = reverse_lazy('operations:get_money')

    @atomic
    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        amount = cleaned_data.get('amount')
        if amount > self.card.balance:
            messages.error(self.request,
                           u'Сумма превышает количество доступных \
                           средств на балансе карты')
            return redirect_to_messages_url(self.fail_url)

        operation = GetMoneyOperation.objects.create(card=self.card,
                                                     amount=amount)
        self.card.balance = self.card.balance - amount
        self.card.save()

        return redirect(reverse(self.success_url, kwargs={'pk': operation.pk}))


class GetMoneySuccessView(CardMixin, SingleObjectMixin, TemplateView):
    """ Страница о результате операции """

    template_name = "operations/get_money_success.html"

    def get_queryset(self):
        return self.card.getmoneyoperation_set.all()

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        return super(GetMoneySuccessView, self).get_context_data(**kwargs)
