from django.conf.urls import url
from operations.views import (OperationsListView, OperationsLogout,
                              BalanceView, GetMoneyView,
                              GetMoneySuccessView)

urlpatterns = [
    url(r'^$', OperationsListView.as_view(), name='operations_list'),
    url(r'^logout/$', OperationsLogout.as_view(), name='logout'),
    url(r'^balance/$', BalanceView.as_view(), name='balance'),
    url(r'^get_money/$', GetMoneyView.as_view(), name='get_money'),
    url(r'^get_money_success/(?P<pk>\d+)/$', GetMoneySuccessView.as_view(),
        name='get_money_success'),
]
