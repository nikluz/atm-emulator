# -*- coding: utf-8 -*-
from django import forms
from django.core.validators import MinValueValidator

from common.widgets import FieldWithDigitalKeyboard


class GetMoneyForm(forms.Form):
    amount = forms.IntegerField(label=u"Сумма:",
                                widget=FieldWithDigitalKeyboard(),
                                validators=[MinValueValidator(1)])
