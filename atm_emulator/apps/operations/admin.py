from django.contrib import admin
from operations.models import ViewBalanceOperation, GetMoneyOperation


admin.site.register(ViewBalanceOperation)
admin.site.register(GetMoneyOperation)
