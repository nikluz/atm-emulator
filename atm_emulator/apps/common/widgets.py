# -*- coding: utf-8 -*-
from django import forms
from django.utils.safestring import mark_safe
from django.template.loader import get_template
from django.template import Context


class FieldWithDigitalKeyboard(forms.HiddenInput):
    template_name = 'common/field_with_digital_keyboard.html'

    def render(self, name, value, attrs=None):
        super_rendered = super(FieldWithDigitalKeyboard, self).render(name, value,
                                                            attrs)

        digital_keys = (
            (7, 8, 9),
            (4, 5, 6),
            (1, 2, 3),
            (None, 0, None),
        )
        template = get_template(self.template_name)
        rendered_html = template.render(Context({
            'super_rendered': super_rendered,
            'digital_keys': digital_keys,
        }))
        return mark_safe(rendered_html)
