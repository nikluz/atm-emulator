# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.shortcuts import redirect


def get_messages_url(url="/"):
    return "%s?back=%s" % (reverse('common:messages'), url)


def redirect_to_messages_url(url="/"):
    return redirect(get_messages_url(url))
