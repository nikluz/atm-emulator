from django.conf.urls import url
from common.views import MessagesView

urlpatterns = [
    url(r'^messages/$', MessagesView.as_view(),
        name='messages'),
]
