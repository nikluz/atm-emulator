# -*- coding: utf-8 -*-
from django.views.generic import TemplateView


class MessagesView(TemplateView):
    """ Страница со списком сообщений """

    template_name = "common/messages.html"

    def get_context_data(self, **kwargs):
        context = super(MessagesView, self).get_context_data(**kwargs)
        context['back_url'] = self.request.GET.get('back', None)
        return context
