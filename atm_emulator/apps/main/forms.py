# -*- coding: utf-8 -*-
from django import forms
from common.widgets import FieldWithDigitalKeyboard


class CardNumberForm(forms.Form):
    card_number = forms.CharField(label=u"Номер банковской карты:",
                                  widget=FieldWithDigitalKeyboard(),
                                  min_length=16, max_length=16)


class PinCodeForm(forms.Form):
    pin_code = forms.CharField(label=u"Пин код банковской карты:",
                               widget=FieldWithDigitalKeyboard(),
                               min_length=4, max_length=4)
