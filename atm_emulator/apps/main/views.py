# -*- coding: utf-8 -*-
from django.views.generic.edit import FormView
from django.views.generic.detail import SingleObjectMixin
from django.contrib import messages
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.conf import settings

from main.forms import CardNumberForm, PinCodeForm
from main.models import Card, PinFailures
from common.utils import redirect_to_messages_url


class RemoveCardPk(object):
    """ Убирает card_pk из сессии, если существует """

    def dispatch(self, request, *args, **kwargs):
        if 'card_pk' in self.request.session:
            del self.request.session['card_pk']
        return super(RemoveCardPk, self).dispatch(request, *args, **kwargs)


class CardNumberFormView(RemoveCardPk, FormView):
    """ Страница с формой ввода номера карты """

    template_name = 'main/card_number_form.html'
    form_class = CardNumberForm
    success_url = 'main:pin_code'

    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        card_number = cleaned_data.get('card_number')
        card = Card.objects.filter(card_number=card_number).first()
        if card is None:
            messages.error(self.request,
                           u'Введенный номер банковской карты не существует')
            return redirect_to_messages_url(reverse('homepage'))
        elif card.is_blocked:
            messages.error(self.request,
                           u'Данная банковская карта заблокирована')
            return redirect_to_messages_url(reverse('homepage'))

        return redirect(reverse(self.success_url, kwargs={'pk': card.pk}))


class PinCodeFormView(RemoveCardPk, FormView, SingleObjectMixin):
    """ Страница с формой ввода пин кода """

    model = Card
    template_name = 'main/pin_code_form.html'
    form_class = PinCodeForm
    success_url = reverse_lazy('operations:operations_list')
    fail_url = 'main:pin_code'
    block_url = reverse_lazy('homepage')

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(PinCodeFormView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super(PinCodeFormView, self).get_queryset().filter(
            is_blocked=False)

    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        pin_code = cleaned_data.get('pin_code')

        if not self.object.check_pin_code(pin_code):
            PinFailures.objects.create(card=self.object)
            fails_count = self.object.pinfailures_set.count()
            if(fails_count >= settings.MAX_PIN_CODE_FAILURES):
                self.object.is_blocked = True
                self.object.save()
                messages.error(self.request,
                               u'Превышено число попыток ввода пин кода. \
                                 Карта заблокирована')
                return redirect_to_messages_url(self.block_url)

            efforts_left = settings.MAX_PIN_CODE_FAILURES - fails_count

            messages.error(self.request,
                           u'Введен неверный пин код. Осталось попыток: %s' %
                           efforts_left)
            return redirect_to_messages_url(reverse(self.fail_url,
                            kwargs={'pk': self.object.pk}))

        self.request.session['card_pk'] = self.object.pk

        return super(PinCodeFormView, self).form_valid(form)
