from django.conf.urls import url
from main.views import PinCodeFormView

urlpatterns = [
    url(r'^pin_code/(?P<pk>\d+)/$', PinCodeFormView.as_view(),
        name='pin_code'),
]
