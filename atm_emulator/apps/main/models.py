# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models.signals import pre_save
from django.contrib.auth.hashers import (make_password, check_password,
                                         is_password_usable)


class Card(models.Model):
    """
    Банковские карточки

    Поля:
        card_number - номер банковской карты
        pin_code - хэшированный пин код банковской карты
        balance - баланс (только позитивные значения)
        is_blocked - True, если карта заблокирована
    """
    card_number = models.CharField(verbose_name=u'Номер карты', max_length=16,
                                   unique=True)
    pin_code = models.CharField(verbose_name=u'Пин код', max_length=100)
    balance = models.PositiveIntegerField(verbose_name=u'Баланс', default=0)
    is_blocked = models.BooleanField(verbose_name=u'Заблокирована',
                                     default=False)

    @property
    def card_number_formatted(self):
        """Возвращает отформатированный номер банковской карточки"""
        if not self.card_number:
            return None
        return '-'.join(
            [self.card_number[i:i+4]
                for i in range(0, len(self.card_number), 4)])

    @staticmethod
    def generate_pin_code(pin_code):
        return make_password(pin_code)

    def set_pin_code(self, pin_code):
        self.pin_code = self.generate_pin_code(pin_code)

    def check_pin_code(self, pin_code):
        return check_password(pin_code, self.pin_code)

    @staticmethod
    def pre_save(sender, instance, **kwargs):
        if not is_password_usable(instance.pin_code):
            instance.pin_code = sender.generate_pin_code(instance.pin_code)

        return instance

    def __unicode__(self):
        return "%s (%s)" % (
            self.card_number_formatted,
            u"Заблокирована" if self.is_blocked else u"Активна",
        )

    class Meta:
        verbose_name = u"банковская карта"
        verbose_name_plural = u"банковские карты"


pre_save.connect(Card.pre_save, Card)


class PinFailures(models.Model):
    card = models.ForeignKey(Card, verbose_name=u"Банковская карта")
    date_time = models.DateTimeField(auto_now=True, verbose_name=u"Дата")

    class Meta:
        verbose_name = u"ошибка ввода пин кода"
        verbose_name_plural = u"ошибки ввода пин кода"
