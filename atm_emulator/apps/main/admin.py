from django.contrib import admin
from main.models import Card, PinFailures


admin.site.register(Card)
admin.site.register(PinFailures)
